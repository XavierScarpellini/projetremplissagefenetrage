#pragma once
#include "struct.h"
#include "Line.h"
#include <stack>
#include <iostream>
using namespace std;

class Shape
{
public:
	Shape();
	Shape(int, int, int, int, color);
	~Shape();
	void SetPoint(int x, int y);
	void Draw();
	void Fill(int x, int y);
	void LCA(int x, int y);
	void LCA2(int x, int y);
	void LCAFill();
	Line* Shape::GetActiveLines(int y);
	void (Shape::*_draw)();
	void DisplayPoints();

	Line lines[5] = {};
	point* pointList;
	point* PointsToList();
	color _color;
	int LineCounter;
	bool lca;
	int isInsideTheShape(int x, int y);
	void ResetLinesToTest();


private:
	Line* GetLinesToTest();
	point GetCoordsMin();
	point GetCoordsMax();
	point* LCAPointList;
	bool isPixelOnBorder(int x, int y);
	bool _canBeDrawn;
	bool _firstClic;
	int _lastLineIndex;
};
