#pragma once
#include "struct.h"

class Line
{
public:
	Line();
	Line(int, int, int, int, color);
	~Line();

	void setSecondPoint(int x, int y);
	int absolute(int value);
	void Draw();
	void Draw(color c);
	point* GetPoints();
	point* GetPointsReverse();
	bool DoContaineThisPoint(int x, int y);

	void (Line::*_draw)();
	point* (Line::*_getLinePoints)();
	point* p1;
	point* p2;
	int xMin;
	int xMax;
	int yMin;
	int yMax;
	color _color;
	bool hasBeenCrossed;
	point* pointList;
	Line* next;

private:	
	point fillPoint;
	int incX;
	int incY;
	float _m;
	float _p;
};

