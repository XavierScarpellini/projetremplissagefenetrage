#include "Stack.h"
#include <glew.h>
#include <iostream>

/*
t_list* stack = NULL;
stack_push(&stack, 12);
stack_push(&stack, 5);
printf(stack_pop(&stack));
printf(stack_peek(&stack));
*//**/
Stack::Stack(point p) {
	stack->_point = p;
	stack->next = NULL;
}

void Stack::Push(t_list** stack, point p) {
	t_list* newMember;
	newMember->_point = p;
	newMember->next = *stack;
	*stack = newMember;
}

point Stack::Peek(t_list** stack) {
	if (*stack)
		return (*stack)->_point;
	return NULL;
}

point Stack::Pop() {
	if (!stack) {
		return NULL;
	}
	point tmp = stack->_point;
	t_list* remove = stack;
	stack = stack->next;
	free(remove);
	return tmp;
}