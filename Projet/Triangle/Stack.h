#include "struct.h"
#include "Line.h"

typedef struct s_list {
	point _point;
	struct s_list * next;
}t_list;


class Stack
{
public:
	Stack(point p);
	void Stack::Push(t_list** stack, point value);
	point Stack::Peek(t_list** stack);
	point Stack::Pop();

private:
	t_list* stack;
};