#pragma once
#include "struct.h"
#include "Shape.h"

class Clipping {
public:
	Clipping();

	bool   IsPointInside(point* p1, point* edge);
	void   Clip(point* polygon, point* window);
	point* GetLastElement(point* pointList);
	point* ComputeIntersection(point* start, point* end, point* edge);
	float  DotProduct(point* p1, point* edge);
};
