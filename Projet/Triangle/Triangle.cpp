#include <cmath>
#include "Line.h"
#include "Shape.h"
#include "Fill.h"
#include "Clipping.h"
//#include "PointList.h"

Shape polygon;
Shape window;
Clipping* clipTest;
point* windowPoints;
point* polygonPoints;
point* edge;
Line* L;
Shape S[10];
Fill option;

int cur = 0, CMax = 0, LMax = 0, circle_on = 0, trim_on = 0;
int currentShapeIndex = 0;
bool firstClic = true;
bool LEFT_CLIC_PRESSED = false;
int enabledMode = 0;

void setPixel(GLint x, GLint y, color c)
{
	glBegin(GL_POINTS);
	glColor3f(c._r, c._g, c._b);
	glVertex2i(x, y);
	glEnd();
}

color getPixel(int x, int y) {
	float pixelf[3];
	glReadPixels(x, WINDOW_HEIGHT - y, 1, 1, GL_RGB, GL_FLOAT, pixelf);
	glFlush();
	return color{ pixelf[0], pixelf[1], pixelf[2] };
}

void mouse(int button, int state, int x, int y)
{
	int i = cur;
	int R = 100;
	int V = 150;
	int B = 200;
	int Form = 300;


	if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN) {
		if (false) {
			if (firstClic) {
				L[currentShapeIndex].p1->x = x;
				L[currentShapeIndex].p1->y = y;
				firstClic = false;
			} else {
				L[currentShapeIndex].setSecondPoint(x, y);
				currentShapeIndex++;
				firstClic = true;
			}
		} else {
			S[0]._color = color{ 125, 125, 0 };
			S[0].lca = false;
			S[0].SetPoint(x, y);
		}
	}
	if (button == GLUT_RIGHT_BUTTON && state == GLUT_DOWN) {
		/*
		if (clipTest->IsPointInside(new point{ x, y }, edge)) {
			point* displayPoint = clipTest->ComputeIntersection(new point{ x, y }, new point{ 10, 100 }, edge);
			std::cout << "Point: " << x << " : " << y << std::endl;
			std::cout << "newPoint: " << displayPoint->x << " : " << displayPoint->y << "\n" << std::endl;


			setPixel(x, y, color{ 1, 1, 1 });
			setPixel(displayPoint->x, displayPoint->y, color{ 1, 1, 1 });
			glFlush();
		}
		*/
		
		/*
		polygon.SetPoint(150, 90);
		polygon.SetPoint(450, 210);
		polygon.SetPoint(300, 560);
		*/

		S[0].DisplayPoints();
		clipTest->Clip(S[0].pointList, window.pointList);


		//std::cout << clipTest->DotProduct(new point{ x, y }, polygon->p1) << std::endl;
		//S[0].ResetLinesToTest();
		//S[0]._color = color{ 125, 125, 0 };
		//S[0].Fill(x, y);
		//S[0].LCAFill();
		//fill.Remplissage(x, y, color{ 1, 0, 1 }, color{ 1, 1, 1 });
	}
	if (button == GLUT_MIDDLE_BUTTON && state == GLUT_DOWN) {
		//S[0].Fill(x, y);
		//S[0].LCA2(x, y);
		S[0].LCA(x, y);

		//fill.GetPixel(x, y);
		//fill.Remplissage(x, y, color{ 1, 0, 1 }, color{ 1, 1, 1 });
	}
}

void drawRepaire() {

	for(int x = 0; x < WINDOW_WIDTH; x++)	{
		setPixel(x, 1, color{ 1, 0, 0});
	}
	for (int y = 0; y < WINDOW_HEIGHT; y++) {
		setPixel(0, y, color{ 1, 0, 0});
	}
}

void Display() {
	glClear(GL_COLOR_BUFFER_BIT);
	drawRepaire();

	//	polygon.Draw();
		window.Draw();
	//L->Draw();
	//setPixel(10, 100, color{ 1, 1, 1 });
	/*
	for (int i = 0; i < currentShapeIndex; i++) {	
		L->Draw();
	}*/
	S[0].Draw(); 

	glFlush();
}

int main(int argc, const char* argv)
{
	
	edge = new point{ 300, 500 };
	edge->next = new point{ 200, 100 };


	L = new Line( 300, 500, 200, 100, color{0,1,0.5} );
	currentShapeIndex++;

	clipTest = new Clipping();
	//polygon = Shape();
	polygon._color = color{ 1, 1, 0 };/*
	polygon.SetPoint(150, 90);
	polygon.SetPoint(450, 210);
	polygon.SetPoint(300, 560);
	/*
	polygonPoints = new point{ 150, 90 };
	polygonPoints.next = new point{ 450, 210 };
	polygonPoints.next.next = new point{ 300, 560 };
	
	window = Shape();
	window._color = color{ 0, 1, 1 };
	*/
	window.SetPoint(90, 90);
	window.SetPoint(600, 120);
	window.SetPoint(500, 400);
	window.SetPoint(200, 350);

	glutInit(&argc, (char **)argv);
	//glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowPosition(1000, 100);
	glutInitWindowSize(WINDOW_WIDTH, WINDOW_HEIGHT);
	glutCreateWindow("Projet Math");


	glClearColor(0.0, 0.0, 0.0, 1.0);
	glClear(GL_COLOR_BUFFER_BIT);
	glPointSize(1.0);
	gluOrtho2D(0, WINDOW_WIDTH, WINDOW_HEIGHT, 0);

	//glutIdleFunc(init);
	glutDisplayFunc(Display);
	glutMouseFunc(mouse);
	//glutMotionFunc(mousemotion);
	//glutKeyboardFunc(KeyboardControl);


	glutMainLoop();

	return 0;
}