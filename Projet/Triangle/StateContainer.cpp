#include <string>

class StateContainer {
	
	public :
		StateContainer() {
			_drawLine = true;
			_drawShape = false;
			_drawCircle = false;
			_trim = false;
		}

		void setState(int MODE) {
			_drawLine = false;
			_drawShape = false;
			_drawCircle = false;
			_trim = false;

			switch (MODE) {
			case 0:		//"LINE":
				_drawLine = true;
				break;
			case 1:		//"SHAPE":
				_drawShape = true;
				break;
			case 2:		//"CIRCLE":
				_drawCircle = true;
				break;
			case 3:		//"TRIM":
				_trim = true;
				break;
			default:
				_drawLine = true;
				break;
			}
		}

	private :
		bool _drawLine;
		bool _drawShape;
		bool _drawCircle;
		bool _trim;
};
