#pragma once
#ifdef _MSC_VER
#pragma comment(lib, "opengl32.lib")
#include <Windows.h>
#endif

#ifdef NO_GLEW
#include <gl/GL.h>
#include "glext.h"
#else
#include "glew.h"
#ifdef _MSC_VER
#pragma comment(lib, "glew32.lib")
#endif
#endif // !NO_GLEW

#include "freeglut.h"
#include <iostream>
#include "../common/EsgiShader.h"
#include <list>

#define WINDOW_HEIGHT 600
#define WINDOW_WIDTH 800
/*
typedef struct {
public:
	int ymax;
	float xmin;
	float invertCoef;
	Node* next;

	void Node() {
		ymax = 0;
		xmin = 0;
		invertCoef = 0;
	}
}Node;

typedef struct {
	public:
		Node** SI;


}SI;
*/
typedef struct
{
	float _r;
	float _g;
	float _b;

	public: bool isDifferent(float r, float g, float b) {
		return (r != _r || g != _g || b != _b);
	}
}color;


typedef struct _p
{
public:
	int x;
	int y;
	struct _p *next;

	void point(int _x, int _y) {
		x = _x;
		y = _y;
		next = 0;
	}

	void set(int _x, int _y) {
		x = _x;
		y = _y;
	}
	
	bool isDifferent(int xToCompare, int yToCompare) {
		if (xToCompare == x && yToCompare == y) {
			return false;
		}
		return true;
	}

	struct _p* GetLastPoint() {
		struct _p* point = this;
		while (point) {
			point = point->next;
		}
		return point;
	}

	struct _p* GetPreviousLastPoint() {
		struct _p* point = this;
		while (point->next) {
			point = point->next;
		}
		return point;
	}
	
	static void Free(struct _p* list) {
		struct _p* tmp;
		while (list->next) {
			tmp = list->next;
			free(list);
			list = tmp;
		}
	}
}point;

void setPixel(int, int, color);
color getPixel(int, int);