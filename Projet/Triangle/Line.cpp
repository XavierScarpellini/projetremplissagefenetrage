#include "Line.h"

Line::Line()
{
	p1 = new point;
	p2 = new point;
	p1->next = p2;
	p1->x = 0;
	p2->x = 0;
	p1->y = 0;
	p2->y = 0;
	_color = color{255, 255, 255};
}


Line::Line(int x1, int y1, int x2, int y2, color c)
{
	p1 = new point;
	p2 = new point;
	p1->next = p2;
	p1->x = x1;
	p1->y = y1;
	setSecondPoint(x2, y2);
	_color = c;
}

Line::~Line()
{
}

int Line::absolute(int value) {
	return value < 0 ? -value : value;
}

void Line::setSecondPoint(int x, int y) {
	p2->x = x;
	p2->y = y;

	p1->x < p2->x ? (xMin = p1->x, xMax = p2->x) : (xMin = p2->x, xMax = p1->x);
	p1->y < p2->y ? (yMin = p1->y, yMax = p2->y) : (yMin = p2->y, yMax = p1->y);
	
	_m = p2->x - p1->x != 0 ? (float)(p2->y - p1->y) / (float)(p2->x - p1->x) : 0;
	_p = (float)(p1->y-(_m*p1->x));

	incX = p2->x < p1->x ? -1 : 1;
	incY = p2->y < p1->y ? -1 : 1;
	
	if (absolute(p2->x - p1->x) >= absolute(p2->y - p1->y))	{
		pointList = GetPoints();
	}
	else {
		pointList = GetPointsReverse();
	}
}

void Line::Draw() {
	point* point = pointList;
	while (point) {
		setPixel(point->x, point->y, _color);
		point = point->next;
	}
}

void Line::Draw(color c) {
	point* point = pointList;
	while (point) {
		setPixel(point->x, point->y, c);
		point = point->next;
	}
}

point* Line::GetPoints() {
	int dx = absolute(p2->x - p1->x);
	int dy = absolute(p2->y - p1->y);
	int incE = 2 * dy;
	int incNE = 2 * (dy - dx);
	int di = 2 * dy - dx;
	int x = 0, y = p1->y;
	point* firstPoint;
	firstPoint = new point;

	point* currentPoint = new point;
	firstPoint = currentPoint;
	for (x = p1->x; x != p2->x; x += incX)
	{
		currentPoint->x = x;
		currentPoint->y = y;
		currentPoint->next = new point;

		currentPoint = currentPoint->next;

		if (di <= 0) {
			di += incE;
		}
		else {
			di += incNE;
			y += incY;
		}
	}
	currentPoint->x = p2->x;
	currentPoint->y = p2->y;
	currentPoint->next = NULL;
	
	return firstPoint;
}

point* Line::GetPointsReverse() {
	int dx = absolute(p2->x - p1->x);
	int dy = absolute(p2->y - p1->y);
	int incE = 2 * dx;
	int incNE = 2 * (dx - dy);
	int di = 2 * dx - dy;
	int y = 0;
	int x = p1->x;
	point* firstPoint;
	firstPoint = new point;

	point* currentPoint = new point;
	firstPoint = currentPoint;
	for (y = p1->y; y != p2->y; y += incY){
		currentPoint->x = x;
		currentPoint->y = y;
		currentPoint->next = new point;

		currentPoint = currentPoint->next;
		if (di <= 0) {
			di += incE;
		} else {
			di += incNE;
			x += incX;
		}
	}
	currentPoint->x = p2->x;
	currentPoint->y = p2->y;
	currentPoint->next = NULL;

	return firstPoint;
}

bool Line::DoContaineThisPoint(int x, int y) {
	
	point* linePoints = pointList;


	while (linePoints) {
		if (!linePoints->isDifferent(x, y)) {
			return true;
		}
		linePoints = linePoints->next;
	}
	return false;
}