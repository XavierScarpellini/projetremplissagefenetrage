#include "Clipping.h"
#include "Line.h"
#include <list>

Clipping::Clipping() {

}

void Clipping::Clip(point* shape, point* clipPoly) { //PL PW
	point* inputList = shape;
	point* firstPointPointer = shape;
	bool firstPoint = true;
	point* previousPoint;
	point* outputList;
	point* outputListFistElement;
	point edge[2];
	while (clipPoly->next) {
		edge[0] = *clipPoly;
		edge[1] = *clipPoly->next;
		std::cout << "edge:\n" << edge[0].x << ", " << edge[0].y << std::endl;
		std::cout << edge[1].x << ", " << edge[1].y << std::endl;

		outputList = new point{-1, -1};
		outputListFistElement = outputList;
		while (inputList) {
			std::cout << "    point: " << inputList->x << ", " << inputList->y << std::endl;
			if (firstPoint) { //Save firstpoint
				previousPoint = new point{ inputList->x, inputList->y };
				outputList->set(inputList->x, inputList->y);
				outputList->next = new point;
				outputList = outputList->next;
			} else {
				bool firstPointIsInside = IsPointInside(firstPointPointer, edge);
				bool secondPointIsInside = IsPointInside(previousPoint, edge);

				if ((firstPointIsInside && !secondPointIsInside) || (!firstPointIsInside && secondPointIsInside)) {
					outputList = ComputeIntersection(previousPoint, inputList, edge);
					outputList->next = new point;
					outputList = outputList->next;
				}
			}
			previousPoint->set(inputList->x, inputList->y);
			if (IsPointInside(inputList, edge)) {
				outputList->set(previousPoint->x, previousPoint->y);
				outputList->next = new point;
				outputList = outputList->next;				
			}
			firstPoint = false;
			inputList = inputList->next;
		}
		if (outputListFistElement->x != -1) {
			bool firstPointIsInside = IsPointInside(firstPointPointer, edge);
			bool secondPointIsInside = IsPointInside(previousPoint, edge);

			if ((firstPointIsInside && !secondPointIsInside) || (!firstPointIsInside && secondPointIsInside)) {
				outputList = ComputeIntersection(previousPoint, firstPointPointer, edge);
				outputList->next = new point;
				outputList = outputList->next;
			}
			inputList = outputListFistElement;
		}
		clipPoly = clipPoly->next;
	}
}

point* Clipping::GetLastElement(point* pointList) {
	while (pointList->next) {
		pointList = pointList->next;
	}
	return pointList;
}

bool Clipping::IsPointInside(point* p1, point* edge) {
	if (DotProduct(p1, edge) > 0) {
		return false;
	}
	return true;
}

float Clipping::DotProduct(point* p1, point* edge) {
	float m = edge->next->x - edge->x != 0 ? (float)(edge->next->y - edge->y) / (float)(edge->next->x - edge->x) : 0;	

	return -m*(p1->x - edge->x) + (p1->y - edge->y);
}

point* Clipping::ComputeIntersection(point* p1, point* p2, point* edge) {
	
	float B[2] = { { edge->x - p1->x },
				 { edge->y - p1->y } };
	
	float A[2][2] = { {p2->x - p1->x , edge->x - edge->next->x },
					  {p2->y - p1->y , edge->y - edge->next->y } };
	
	//std::cout << "B: " << B[0] << ", " << B[1] << std::endl;
	/*
	std::cout << A[0][0] << ", " << A[0][1] << std::endl;
	std::cout << A[1][0] << ", " << A[1][1] << std::endl;
	*/
	float detA = A[0][0] * A[1][1] - A[0][1] * A[1][0];

	if (detA!=0) {
		//std::cout << "determinant: " << detA << std::endl;

		float comA[2][2] = { {  A[1][1] / detA, -A[0][1] / detA },
							 { -A[1][0] / detA,  A[0][0] / detA } };
		
		//std::cout << "comB: \n" << comA[0][0] << ", " << comA[0][1] << std::endl;
		//std::cout << comA[1][0] << ", " << comA[1][1] << '\n' << std::endl;
		
		float X[2] = { (comA[0][0] * B[0] + comA[0][1] * B[1]), (comA[1][0] * B[0] + comA[1][1] * B[1]) };

		//std::cout << comA[1][0] << " x " << B[0] << " + " << comA[1][1] << "x" << B[1] << "=" << X[1] << std::endl;
		//std::cout << X[0] << ", " << X[1] << std::endl;

		auto Px = p1->x + (float)(p2->x - p1->x) * X[0];
		auto Py = p1->y + (float)(p2->y - p1->y) * X[0];
		
		//auto P2x = edge->x + (float)(edge->next->x - edge->x) * X[1];
		//auto P2y = edge->y + (float)(edge->next->y - edge->y) * X[1];

		//std::cout << P2x << ", " << P2y << std::endl;

		return new point{(int)Px, (int)Py};
	}

	return p2;
}