#include "Shape.h"

Shape::Shape()
{
	_canBeDrawn = false;
	_firstClic = true;
	_lastLineIndex = 0;
	LineCounter = 1;
	lca = false;
}

Shape::Shape(int x1, int x2, int y1, int y2, color c)
{
	_canBeDrawn = false;
	_firstClic = true;
	_lastLineIndex = 0;
	LineCounter = 1;
	lca = false;
}

Shape::~Shape()
{
	LineCounter = 1;
	lca = false;
}

void Shape::SetPoint(int x, int y) {
	if (_lastLineIndex < 4) {
		if (_firstClic) {
			lines[_lastLineIndex].p1->x = x;
			lines[_lastLineIndex].p1->y = y;
			_firstClic = false;
		} else {
			lines[_lastLineIndex].setSecondPoint(x, y);

			_lastLineIndex++;
			lines[_lastLineIndex].p1->x = x;
			lines[_lastLineIndex].p1->y = y;
			lines[_lastLineIndex].setSecondPoint(lines[0].p1->x, lines[0].p1->y);

			LineCounter++;

		}
		if (pointList) {
			std::cout << "in" << std::endl;
			point* tmp = pointList;
			while (tmp->next) {
				tmp = tmp->next;
			}
			tmp->next = new point{ x, y };
		} else {
			pointList = new point{ x, y };
		}

	}
}

void Shape::DisplayPoints() {
	point* tmp = pointList;
	while (tmp) {
		std::cout << tmp->x <<" " <<  tmp->y << std::endl;
		tmp = tmp->next;
	}
}

void Shape::Draw() {
	if (_lastLineIndex > 0) {
		for (int i = 0; i <= _lastLineIndex; i++) {
			lines[i].Draw(_color);//(lines[i].*lines[i]._draw)();
		}
	}
	if (lca) {
		LCAFill();
	}
	point* point = pointList;
	while (point) {
		setPixel(point->x, point->y, color{ 1, 0, 1 });
		point = point->next;
	}
}

void Shape::Fill(int x, int y) {
	bool isInside;
	Line line; 
	point* pointLine;
	line.p1->x = x;
	line.p1->y = y;
	line._color = color{ 255, 255, 0 };
	int originX = 10;

	for (int originX = 0; originX <= WINDOW_WIDTH; originX++) {
		ResetLinesToTest();
		line.setSecondPoint(originX, 0);
		pointLine = line.pointList; //(line.*line._getLinePoints)();
		isInside = true;
		while (pointLine->next != NULL || pointLine == NULL) {
			if (isInsideTheShape(pointLine->x, pointLine->y) % 2 == 1) {
				isInside = isInside ? false : true;
			}
			if (isInside) {
				setPixel(pointLine->x, pointLine->y, _color);
			}
			pointLine = pointLine->next;
		}
	}
	for (int originY = 0; originY <= WINDOW_HEIGHT; originY++) {
		ResetLinesToTest();
		line.setSecondPoint(WINDOW_WIDTH, originY);
		pointLine = line.pointList; //(line.*line._getLinePoints)();
		isInside = true;
		while (pointLine != NULL) {
			if (isInsideTheShape(pointLine->x, pointLine->y) % 2 == 1) {
				isInside = isInside ? false : true;
			}
			if (isInside) {
				setPixel(pointLine->x, pointLine->y, _color);
			}
			pointLine = pointLine->next;
		}
	}
	for (int originX = 0; originX <= WINDOW_WIDTH; originX++) {
		ResetLinesToTest();
		line.setSecondPoint(originX, WINDOW_HEIGHT);
		pointLine = line.pointList; //(line.*line._getLinePoints)();
		isInside = true;
		while (pointLine != NULL) {
			if (isInsideTheShape(pointLine->x, pointLine->y) % 2 == 1) {
				isInside = isInside ? false : true;
			}
			if (isInside) {
				setPixel(pointLine->x, pointLine->y, _color);
			}
			pointLine = pointLine->next;
		}
	}
	for (int originY = 0; originY <= WINDOW_HEIGHT; originY++) {
		ResetLinesToTest();
		line.setSecondPoint(0, originY);
		pointLine = line.pointList; //(line.*line._getLinePoints)();
		isInside = true;
		while (pointLine != NULL) {
			if (isInsideTheShape(pointLine->x, pointLine->y) % 2 == 1) {
				isInside = isInside ? false : true;
			}
			if (isInside) {
				setPixel(pointLine->x, pointLine->y, _color);
			}
			pointLine = pointLine->next;
		}
	}
	glFlush();
}

void Shape::LCA2(int x, int y) {
	point coordsMin = GetCoordsMin();
	point coordsMax = GetCoordsMax();

	point** SI = new point*[coordsMax.y - coordsMin.y + 1];
	//Line* activeLines;

	for (int yCurrent = coordsMin.y; yCurrent <= coordsMax.y; yCurrent++) {
		//activeLines = GetActiveLines(yCurrent);
		SI[yCurrent] = new point;
		ResetLinesToTest();
		for (int xCurrent = coordsMin.x; xCurrent <= coordsMax.x; xCurrent++) {
			if (isInsideTheShape(xCurrent, yCurrent) % 2 == 1) {
				setPixel(xCurrent, yCurrent, color{ 1, 0, 0 });
				glFlush();
				std::cout << 1 << std::endl;
				SI[yCurrent]->set(xCurrent, yCurrent);
				std::cout << 2 << std::endl;
				SI[yCurrent]->next = new point;
				std::cout << 3 << std::endl;
				SI[yCurrent] = SI[yCurrent]->next;
				std::cout << 4 << std::endl;
			}
		}
	}
}

Line* Shape::GetActiveLines(int y) {
	Line* activeLines = new Line;
	for (int i = 0; i <= _lastLineIndex; i++) {
		if (lines[i].yMin <= y) {
			activeLines = &(lines[i]);
			activeLines->next = new Line;
			activeLines = activeLines->next;
		}
	}
	return activeLines;
}

void Shape::LCA(int x, int y) {
	point coordsMin = GetCoordsMin();
	point coordsMax = GetCoordsMax();
	point** Lca = new point*[coordsMax.y - coordsMin.y];
	point* pointList = new point;
	LCAPointList = pointList;
	bool draw = false;

	for (int yCurrent = coordsMin.y; yCurrent <= coordsMax.y; yCurrent++) {
		glFlush();
		ResetLinesToTest();
		for (int xCurrent = coordsMin.x; xCurrent <= coordsMax.x; xCurrent++) {
			if (draw) {
				setPixel(xCurrent, yCurrent, color{ 0, 0, 1 });
			}
			if (isInsideTheShape(xCurrent, yCurrent) % 2 == 1) {
				pointList->set(xCurrent, yCurrent);
				pointList->next = new point;
				pointList = pointList->next;
				draw = draw ? false : true;

				setPixel(xCurrent, yCurrent, color{ 1, 0, 0 });
			}
		}
	}
	//lca = true;
}

void Shape::LCAFill() {
	while (LCAPointList->next) {
		if (LCAPointList->y == LCAPointList->next->y) {
			for (int x = LCAPointList->x; x < LCAPointList->next->x; x++) {
				setPixel(LCAPointList->x, LCAPointList->y, color{0, 1, 1});
				glFlush();
			}
		}
		LCAPointList = LCAPointList->next;
	}
}

int Shape::isInsideTheShape(int x, int y) {
	int linesCrossedCounter = 0;
	for (int i = 0; i <= _lastLineIndex; i++) {
		if (!lines[i].hasBeenCrossed && lines[i].DoContaineThisPoint(x, y)) {
			lines[i].hasBeenCrossed = true;
			linesCrossedCounter++;
		}
	}
	return linesCrossedCounter;
}

void Shape::ResetLinesToTest() {
	for (int i = 0; i <= _lastLineIndex; i++) {
		lines[i].hasBeenCrossed = false;
	}
}

point Shape::GetCoordsMin() {
	point coordsMin = { lines[0].xMin, lines[0].yMin };
	for (int i = 0; i < LineCounter; i++) {
		if (lines[i].xMin < coordsMin.x) {
			coordsMin.x = lines[i].xMin;
		}
		if (lines[i].yMin < coordsMin.y) {
			coordsMin.y = lines[i].yMin;
		}
	}
	return coordsMin;
}

point Shape::GetCoordsMax() {
	point coordsMax = { lines[0].xMax, lines[0].yMax };
	for (int i = 0; i < LineCounter; i++) {
		if (lines[i].xMax > coordsMax.x) {
			coordsMax.x = lines[i].xMax;
		}
		if (lines[i].yMax > coordsMax.y) {
			coordsMax.y = lines[i].yMax;
		}
	}
	return coordsMax;
}